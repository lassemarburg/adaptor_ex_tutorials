adaptor:ex tutorials
====================

User tutorials and documentation introducing basic functions of adaptor eX client and server. Find the static website created from this repository here:

https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/

Since the current contributing developers are all native german speakers we will start making those in German language and add translations as needed and possible.

Tutorials and docs are in the [docs](./docs) directory. A pipeline is active that recreates the documentation website on each commit to the main branch.

After commit, check if the pipeline did pass.

> The job will fail if there are broken links in any of the markdown files!

If you add a new page don't forget to add it to the navigation tree. Page Navigation is defined in the [mkdocs.yml](./mkdocs.yml) configuration file.

These documentations are hosted with [gitlab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) using [mkdocs](https://gitlab.com/pages/mkdocs) with [material theme](https://github.com/squidfunk/mkdocs-material). Check the mkdocs [material theme documentation](https://squidfunk.github.io/mkdocs-material/reference/abbreviations/) for details on markdown styling options.

Contributing
------------

We are very much open to contributions. This repository is a great starting place to do just that!

Because here you can learn the basic usage und functions of adaptor:ex and tell us, what you did not understand, would need more explanations on etc.

Feel free to submit issues or (even better): fork this repository, add the changes you think make sense and open a merge request.

License
--------

Source code licensed MIT

Authors and acknowledgment
---------------------------

This is a machina commons project by [machina eX](https://machinaex.com).

<br/>

gefördert durch

<img src="https://machinacommons.world/img/Logo_Senat_Berlin.png" alt="Logo Senatsverwaltung für Kultur und Europa Berlin" width="50%"/>

Senatsverwaltung für Kultur und Europa Berlin

