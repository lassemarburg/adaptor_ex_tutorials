Getting Started
=========================

![adaptor:ex](./assets/header.png)

Hier findest du mehr darüber hinaus, wie du mit adaptor:ex deine Storyline, Game Mechanik, Software und Hardware miteinander verdrahtest und unter Kontrolle bringst.

Beginne mit einem [**Tutorial**](./tutorials), wenn du eine der verschiedenen Möglichkeiten kennenlernen willst auf die du adaptor:ex zu nutzen kannst.

Installation
------------

Folge den Anweisungen der [adaptor:ex server readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#install), um das Setup auf deinem lokalen Rechner oder einem Server einzurichten.

Erste Schritte
---------------

### Ein Game erstellen

Gebe die URL, also die webadresse, deines adaptor:ex setups im Web Browser ein. 

Wenn du adaptor:ex mit den Standardeinstellungen auf deinem Rechner oder Laptop installiert hast, kannst du auch auf diesen link klicken: [http://localhost:8080](http://localhost:8080)

![Die adaptor:ex Startseite](./assets/no_games.png)

Beginne damit, ein neues Game Projekt anzulegen. Klicke auf `New Game` und gib deinem ersten Game einen Namen.

![Ein neues Game erstellen](./assets/new_game.png)

### Level hinzufügen
Wenn du das neu erstellte Game Projekt anklickst, landest du in der Übersicht, die alle existierenden level anzeigt. Jedes Game kann aus mehreren Leveln bestehen, die auf verschiedenste Art miteinander verknüpft sind.

Erstelle dein erstes Level mit `Add new`.

> Games und Level bekommen eine eigene Web Adresse, über die sie im Web Browser erreichbar sind. Darum darf der Name keine Leerzeichen oder Sonderzeichen enthalten.

Gib dem Level einen Namen und klicke auf `Add new Level`.

![Ein Level hinzufügen](./assets/new_level.png)

### Level bearbeiten

Öffne das level anschließend, indem du auf den Namen klickst. Jetzt befindest du dich im level Editor.

Start (*START*) und Endpunkt (*QUIT*) deines Levels existieren bereits.

![Der Level Editor](./assets/empty_level.png)

Suche aus der linken Seitenleiste, den [actions](./basics/editor#action), die control action [Log Message](./basics/actions/control/log.md) heraus und ziehe sie per Drag and Drop auf die Mitte der Seite.

Es entsteht ein neues [State](./basics/editor#state) Element, dass eine [Log Message](./basics/actions/control/log.md) action enthält (*log_1*). 

![Eine Action hinzufügen](./assets/log_action.png)

Klicke auf die neue action, um sie zu bearbeiten. Schreibe einen kurzen Text in das `message` Eingabefeld und wähle im `level` Dropdown *info* aus.

![Eine Action bearbeiten](./assets/edit_log_action.png)

Um dein Level verständlicher zu gestalten, ist es gut allen States nachvollziehbare Namen zu geben.

> Auch State Namen dürfen keine Leerzeichen beinhalten, da sie in [Variablen](./basics/variables.md) zum Einsatz kommen können

Klicke in die Titelzeile deines Log Message State und benenne ihn um. 

![States umbenennen](./assets/rename_state.png)

Bearbeite auch die [Next](./basics/actions/control/next.md) action *next_1* im *START* State indem du sie anklickst. Wähle dann den neuen State, hier *HelloWorld*, aus dem `next state` Dropdown aus um ihn mit dem *START* State zu verbinden.

![2 States verbinden](./assets/connect_log_action.png)

### Level starten

Wechsel vom Bearbeitungs in den [Live Modus](./basics/live.md), indem du auf den Play button in der oberen rechten Ecke klickst 

![Live Modus Toggle](./assets/live_toggle.png)

Die linke Seitenleiste mit den actions schließt sich und ein Menü öffnet sich an der rechten Seitenleiste.

Klicke in der rechten Seitenleiste auf `Create Session` und dann auf `Start Session` um eine [Session](./basics/live#session) von deinem Level zu erstellen.

![Eine Session starten](./assets/start_session.png)

Wähle die neu gestartete Session in der rechten Seitenleiste aus.

![Eine session auswählen](./assets/select_session.png)

Der *HelloWorld* State ist bereits hervorgehoben.

Wenn wir eine neue Session von einem Level erstellen, wird automatisch der *START* State ausgelöst. Die *next_1* action hat direkt danach zu unserem State *HelloWorld* mit der Log Message weitergeführt.

Öffne die Log Konsole, indem du auf den cursor icon klickst.

![Log Konsole anzeigen](./assets/console_toggle.png)

Die neueste Log Nachricht sollte 'Hello World!' sein.

Indem du auf den *HelloWorld* State klickst, kannst du ihn manuell erneut auslösen. Beobachte die Log Konsole ob die Nachricht erscheint.

### Level automatisieren

Wechsel zurück in den Bearbeitungs Modus, indem du erneut auf den Live Play Button klickst.

Suche die [Timeout](./basics/actions/time/timeout.md) action in der rechten Seitenleiste heraus und ziehe sie auf den *START* State. 

Wenn du die action darüber ziehst wird der State hervorgehoben und zeigt an, dass die action diesem State hinzugefügt wird, anstatt einen neuen State zu erstellen.

> Wenn wir einem State eine action hinzufügen, die auf etwas wartet, wie ein timer der abläuft, ersetzt sie automatisch eine etwaige [Next](./basics/actions/control/next.md) action.

Bearbeite den timeout. Gib eine kleine Zahl Sekunden im `timeout` feld an und wähle den *HelloWorld* State als `next state` aus.

![Einen timeout erstellen](./assets/timeout.png)

Füge auch dem *HelloWorld* State eine Timeout action hinzu. Bearbeite den Timeout und wähle diesmal *Quit* als `next state` aus.

Wenn du nun in den Live Modus wechselst und eine neue Session startest werden die States in festen Zeitabständen nacheinander ausgelöst.

![Mehrere Sessions gleichzeitig](./assets/automated_session.png)

Sobald der *QUIT* State erreicht ist, wird die session beendet und verschwindet aus der rechten Seitenleiste.

Deine erste Session wird sich nicht automatisch beenden. Um sie zu beenden kannst du sie in der rechten Seitenleiste schließen (x) oder den *QUIT* State auslösen wenn du die Session ausgewählt hast.

> Jede Session basiert immer auf der aktuellsten Version des Levels. Wenn jedoch der State, in dem sich eine Session befindet, verändert, muss er erneut ausgelöst werden.

Nächste Schritte
----------------

In der [**Actions Referenz**](./basics/actions) findest du mehr über die verschiedenen Grundbausteine von adaptor:ex heraus.

[**Tutorials**](./tutorials) leiten dich Schritt für Schritt durch Projekte die du mit adaptor:ex umsetzen kannst.

Lerne zum Beispiel, wie du das Telegram plugin einsetzt, um ein messenger Adventure zu entwickeln: [Storytelling mit Telegram und adaptor eX](./tutorials/telegram-basics)

Autoren und Unterstützer
-------------------------

Dies ist ein [machina commons](https://www.machinacommons.world) Projekt von [machina eX](https://machinaex.com).  

gefördert durch

<img src="https://machinacommons.world/img/Logo_Senat_Berlin.png" alt="Logo Senatsverwaltung für Kultur und Europa Berlin" width="50%"/>

Senatsverwaltung für Kultur und Europa Berlin  
