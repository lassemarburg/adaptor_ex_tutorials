Bedingungen Abfragen mit Conditions
===================================

> Please note: We are just about to work on an english version of the documentation. For some of the pages you will be redirected to the german version. Also texts might still be incomplete and could contain mistranslations.

Wesentlicher Bestandteil jedes Games ist die Abfrage von Bedingungen.

Die logic actions [Switch](../basics/actions/logic/switch.md), [On Event](../basics/actions/logic/onEvent.md) und [On Change](../basics/actions/logic/onChange.md) erlauben es zum Beispiel Bedingungen abzufragen. Auch im telegram und im twilio plugin können wir mit conditions den Verlauf des Spiels gestalten.

Wenn dann (If)
---------

In der `if` option formulieren wir eine odere Mehrere Abfragen, die, wenn sie zutreffen, zu unterschiedlichen States führen.

![Switch Condition](./assets/conditions_switch_example.png)

Hier nutzen wir die [Switch](../basics/actions/logic/switch.md) action um die status variable des aktuellen Player abzufragen und zu entscheiden mit welchem State es weiter gehen soll. Wir überprüfen ob der Wert hinter der [Variable](./variables.md) `Player.status` dem text 'beginner' gleicht (equals). Wenn dies der Fall ist, geht es direkt weiter im state 'Welcome'. Da wir weiter nichts festgelegt haben, wäre an dieser Stelle Schluss, wenn `Player.status` nicht 'beginner' sondern einen anderen Wert hat.

Mit `Add condition` können wir aber weitere Bedingungen hinzufügen, die einen anderen oder den selben `next state` zur Folge haben.

> Bedingungen werden immer von oben nach unten abgefragt. Das ist wichtig um sicher zu wissen was passiert, sobald mehrere Bedingungen auf einmal zutreffen

Sonst (Else)
------------

Manche conditions erlauben es außerdem, dass wir angeben, was passieren soll, wenn keine unserer Bedingungen passt.

Füge über den unteren `Settings` button die `else` Option hinzu.

![Else Option](./assets/conditions_switch_example_add_else.png)

Trifft keine der oberen Bedingungen zu, wird der State ausgelöst, der in `else` angegeben ist.

> Im Falle der `switch` action kannst du statt `else` auch die control [next](../basics/actions/control/next.md) action nutzen um anzugeben welcher State als nächstes kommt, wenn keine der Bedingungen zutrifft.

Action variable 'match'
----------------------

Alle conditions speichern den Wert, der die Bedingung erfüllt hat, in der action variable `match`. Du kannst im folgenden State darauf zugreifen. Conditions in messengern erlauben es dir mitunter sogar die match variable in der selben action bereits zu verwenden.

Du kannst mit `[[state.<state_name>.<condition_action_name>.match]]` darauf zugreifen. 

Im obigen Beispiel anhand der [Switch](../basics/actions/logic/switch.md) action würdest du den Wert also so addressieren: `[[state.SelectStatus.switch_1.match]]`.

In den erläuterung zu den Operatoren findest du ggf. genauere Angaben welcher Wert gespeichert wird.

Abfragetypen (Operatoren)
------------------------

Neben der Abfrage `equals`, ob eine Variable einen bestimmten Wert hat, gibt es weitere möglichkeiten Abfragen zu formulieren.

Du kannst für jede `condition` eine andere Form der Abfrage verwenden.

![Abfrage Auswahl](./assets/conditions_switch_example_operators.png)

Nicht in allen actions, die conditions verwenden stehen alle Abfrageformen zur verfügung.

### Equals

Die Bedingung ist erfüllt, wenn die Werte in `value` und `equals` den selben Wert haben.

Ähnlich dem `==` Operator.

Dabei spielen auch Leerzeichen, Absätze und Satzzeichen eine Rolle. Das bedeutet `Hallo, wie gehts?` ist nicht gleich `Hallo,wie gehts?`.

Groß- und Kleinschreibung wird per default nicht berücksichtigt. Das bedeutet `Hallo, wie gehts?` ist gleich `hallo, wie Gehts?`. Setze die `case sensitive` Option auf aktiv um Groß- und Kleinschreibung zu berücksichtigen.

Es ist möglich mehrere `equals` Werte anzugeben. Die Bedingung trifft dann zu, wenn `value` irgend einem der Werte gleicht.

Bei erfüllter Bedingung enthält die `match` action variable den `equals` Wert, der `value` gleicht.

Variablen, die nicht aufgelöst werden können ergeben stets **undefined**. Du kannst `equals` **undefined** setzen um diesen Fall abzufragen. Die Bedingung ist dann erfüllt, wenn die varible in `value` auf einen nicht existierenden Wert oder ein nicht existierendes Item verweist.

### Contains

Die Bedingung ist erfüllt, wenn der `contains` Wert in `value` enthalten ist.

Contains bezieht sich nur auf Texte, nicht auf Numerische Werte.

`gut` ist in `Mir geht's gut.` enthalten und die Bedingung ist erfüllt.

Auch `ht's g` ist in `Mir geht's gut.` enthalten und erfüllt die Bedingung.

Groß- und Kleinschreibung wird wie bei `equals` per default nicht berücksichtigt. Setze die `case sensitive` Option auf aktiv um Groß- und Kleinschreibung zu berücksichtigen.

Es ist möglich mehrere `contains` Werte anzugeben. Die Bedingung trifft dann zu, wenn einer oder mehrere der Werte in `value` enthalten sind.

Bei erfüllter Bedingung enthält die `match` action variable den `contains` Wert, der in `value` enthalten ist.

### Less Than

Die Bedingung ist erfüllt wenn `value` und `less_than` numerische Werte sind und der Wert in value kleiner als der in less_than ist.

Ähnlich dem `<` Operator.

 `1` ist kleiner als `2`

`2` ist NICHT kleiner als `2`

`-5` ist kleiner als `-2`

Beachte, das bei Fließkomma Zahlen statt dess Komma (`,`) ein Punkt verwendet werden muss (`.`)

`1.54` ist kleiner als `2.1`

Es ist möglich mehrere `less_than` Werte anzugeben. Die Bedingung trifft dann zu, wenn einer oder mehrere der Werte kleiner sind als `value`.

Bei erfüllter Bedingung enthält die `match` action variable den `less_than` Wert, der kleiner ist als `value`.

### Greater Than

Die Bedingung ist erfüllt wenn `value` und `greater_than` numerische Werte sind und der Wert in value größer als der in greater_than ist.

Ähnlich dem `>` Operator.

 `2` ist größer als `1`

`2` ist NICHT größer als `2`

`-2` ist größer als `-5`

Beachte, das bei Fließkomma Zahlen statt dess Komma (`,`) ein Punkt verwendet werden muss (`.`)

`2.1` ist größer als `1.54`

Es ist möglich mehrere `greater_than` Werte anzugeben. Die Bedingung trifft dann zu, wenn einer oder mehrere der Werte größer sind als `value`.

Bei erfüllter Bedingung enthält die `match` action variable den `greater_than` Wert, der größer ist als `value`.

### Regular Expression

Die Bedingung ist erfüllt wenn der Reguläre Ausdruck (Regular Expression) mindestens eine Übereinstimmung hat.

Mit Regular Expressions ist es möglich texte auf verschiedenste Arten zu überprüfen. Regular Expressions sind ein beliebtes tool und es gibt viele ressourcen im Netz, die dir helfen können eine Regex für deine Abfrage zu formulieren. Die Seite https://regexr.com/ etwa hat eine ausführliche Anleitung und Test- und Hilfetools zu erstellen von Regular Expressions.

Regex in adaptor conditions sind immer case insensitiv (i) und nicht global (die Regex wird nur bis zum ersten match ausgeführt)

Gibt es eine Übereinstimmung enthält die `match` action variable den match Wert der regular Expression.

#### Beispiel

![Regular Expression Beispiel](./assets/conditions_switch_example_regex.png)

Diese Abfrage stellt fest, ob die variable **secret_code** ein bestimmtes Format erfüllt. Die regex formel **\d{3}-[a-zA-Z]{4}-\d{3}** liefert dann eine Übereinstimmung, wenn der zu überprüfenden Text mit 3 Zahlen (digits) beginnt, dann, nach einem Bindestrich 4 Buchtaben aus dem alphabet (a-z oder A-Z) folgen und sich, nach einem weiteren Bindestrich, wieder 3 Zahlen anschließen.

Ein **secret_code** '392-hgZb-520' etwa würde zum nächsten State **ValidCode** führen. Der Formatierte teil dürfte dabei auch von anderem text umschlossen sein.

### Javascript Function

JS

### Database Query

Query

