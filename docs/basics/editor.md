Der Editor
==========

Hier wird dein Game zusammengebastelt
Im Level Editor gibt es zwei verschiedene Bereiche: die Stage und die Toolbar.

# TOOLBAR

Links findest du die TOOLBAR. Alles, was du hier findest, kannst du mit der Maus direkt auf die Bühne ziehen (via drag and drop).
Die Toolbar gliedert sich in drei Bereiche: ACTIONS ist der wichtigste (s.u.); VARIABLES enthält Daten, die sich im Laufe einer Spiel-Session ändern können; MEDIA enthält Dateien, die wir einbinden können.

# STAGE

Die STAGE ist der größte Bereich im Leveleditor. 
Du kannst mit Scrollen oder Klicken & Ziehen auf der STAGE navigieren.
Auf der STAGE organisierst und verknüpfst du die STATES deines Games. 

## STATE
  
Ein STATE ist ein Zustand, in dem sich dein Level zu einem bestimmten Zeitpunkt befindet, bis es zum nächsten STATE springt.
In einem neuen Level siehst du bereits die zwei STATES, die es in jedem Level geben muss: START und QUIT.
Zu Beginn des Levels landet die Spieler:in immer im START STATE, und das Level endet erst, wenn sie den QUIT STATE erreicht.
STATES lassen sich auf der Stage via drag and drop bewegen: Einfach mit dem Mauszeiger über der Kopfzeile hovern, klicken und dann bewegen.

In den STATES befinden sich ACTIONS. Du kannst einen neuen STATE erstellen, indem du eine ACTION aus der TOOLBAR auf die STAGE ziehst.
Um einen STATE umzubenennen, klicke auf seinen Namen. 
Du kannst einen STATE auch kommentieren, um zum Beispiel eine Notiz für dich oder andere zu hinterlassen. Dazu klicke auf die Sprechblase neben dem Namen. Eine gefüllte Sprechblase zeigt an, dass bereits ein Kommentar geschrieben wurde. Die Kommentare haben keinerlei Auswirkungen auf den Verlauf des GAMES.

### ACTIONS

Actions sind die kleinsten Bausteine für jedes adaptor:ex level. Hier passieren die wirklich wichtigen Dinge.
Damit auf der Bühne auch was passiert, müssen wir ACTIONS von links aus der Toolbar nach rechts auf die Stage ziehen. 

Jeder [State](../basics/editor#state) enthält eine oder mehrere actions.

Finde in der [Actions Referenz](../actions) mehr über die verschiedenen actions heraus.








