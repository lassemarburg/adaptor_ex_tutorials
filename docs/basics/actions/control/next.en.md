Next
====

> Please note: We are just about to work on an english version of the documentation. For some of the pages you will be redirected to the german version. Also texts might still be incomplete and could contain mistranslations.

Mit `next` geht es direkt weiter zum nächsten State.

Ein State mit `next` action kann keine listen actions enthalten, da der State gewechselt wird, bevor die listener abgefragt werden können.

Die `next` action sollte die letzte action im State sein. Folgende actions werden nicht mehr ausgeführt.