Live Modus
==========

Session
-------

Jedes Level kann viele Male gleichzeitig gespielt werden. Diese Live Versionen des Levels können zu unterschiedlichen Zeiten beginnen und ganz unterschiedlich verlaufen, von unterschiedlichen Spielern gespielt werden usw.

Diese 'Live Level' nennen wir Sessions.

Sessions können manuell oder automatisch gestartet und ebenso manuell oder automatisch beendet werden.

