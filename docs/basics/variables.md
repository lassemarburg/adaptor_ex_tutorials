Variablen, Daten, Referenzen
============================

Variablen erlauben es dir, veränderbare Werte in deinen actions einzusetzen. Variablen sind Platzhalter für Texte, Zahlenwerte oder komplexere Daten, die erst in dem Moment einen festen Wert bekommen in dem die action ausgeführt wird.

Variablen werden in adaptor durch zwei umschließende eckige Klammern gekennzeichnet. Das sieht dann z.B. so aus:

`[[Player.name]]`

Im moment, in dem wir unser Level designen ist offen, welcher Spieler (`Player`) das Level spielt und somit ist auch der Name (`name`) noch *variabel*.

Wird dann eine Session des Levels im Live Modus gestartet und die action ausgeführt wird `[[Player.name]]` durch den Namen des Spielers oder der Spielerin ersetzt, der oder die das Level in diesem Falle spielt.

Variablen können in fast allen actions eingesetzt werden. Du kannst eine variable aus dem **VARIABLES** Menü in deine action ziehen oder per Hand formulieren.

In dieser twilio **smsout** action nutzen wir eine variable um die Ansprache zu personalisieren

![Beispiel action twilio smsout](assets/smsout_example.png)

Wird das Level etwa von einer Spielerin gespielt, für die an einer anderen Stelle der Name 'Ada Lovelace' gespeichert wurde sieht die sms dann so aus: `Hello Ada Lovelace! How is it going?`

Es gibt verschiedene Orte in adaptor:ex an denen variable Daten gespeichert werden und verschiedene Wege diese Daten zu *referenzieren* also mit variablen auf diese Daten zu verweisen. Im folgenden findest du heraus welche Daten und Formen von Referenzen es gibt.


Lokale Variablen
----------------

Lokale Variablen sind Daten, die nur für den Verlauf einer Level Session Bestand haben.

Mit der Data action [Set](../basics/actions/data/set.md) kannst du eine lokale variable erstellen und ihr einen Wert zuweisen oder eine existierende verändern.

Z.B. kannst du so an einer Stelle des Levels markieren, ob der Linke oder der Rechte Weg eingeschlagen wurde um am Ende wieder darauf einzugehen.

![set local varible](assets/set_local_variable.png)

Später kann diese variable dann z.B. in einem Text verwendet oder in einer Logic [Switch](../basics/actions/logic/switch.md) action genutzt werden um zu bestimmen mit welchem state es weiter geht.

Collections und Data Items
--------------------------

Jedes Data Item ist ein Datensatz, der verschiedenste Eigenschaften haben kann und einer Sammlung von items (*collection*) zugeordnet ist. Unter `Game config -> collections` findest du die data items, die derzeit in deinem game existieren. Hier kannst du auch neue Items hinzufügen oder die Eigenschaften von bestehenden Items ändern oder eine ganz neue *collection* erstellen.

Data Items können in jedem Level verwendet werden und bleiben bestehen, auch wenn die session eines Levels beendet wurde. Das ist z.B. wichtig, wenn dein game von verschiedenen Spielern gespielt wird, die im Laufe des Spiels unterschiedliche Fortschritte machen, Punkte sammeln, ihren Namen anpassen und so fort.

Plugins wie telegram oder twilio erstellen automatisch neue `player` items, z.B. wenn ein game von einer bisher unbekannten Telefonnumer kontaktiert wird. Deshalb hat jedes adaptor:ex game bereits von Beginn an eine `player` collection.

Es gibt verschiedene Möglichkeiten Data Items in einem Level zu referenzieren.

### Name Reference

Data Items die eine `name` Eigenschaft haben und diese bereits bekannt ist können darüber direkt referenziert werden. 

`[[player.Ada]]`

ist ein Verweis auf das Data Item mit der `name` Eigenschaft `Ada` in der `player` collection.

Um eine eigenschaft des Data Item zu referenzieren hänge sie mit Punkt Notation getrennt hinten an. Etwa:

`Hello Ada, your score is [[player.Ada.score]]`

Achtung: Namen, die so als refernz verwendet werden dürfen keine Leerzeichen enthalten!

### Inline Query

Der Datenbank Typ, der im Hintergrund adaptor:ex mit Daten versorgt erlaubt es, dass wir so genannte *find querys* verwenden können um auf Items zu verweisen. *find query*s können sehr komplex und beeindruckend sein. Meist benötigen wir aber nur sehr einfache querys die leicht zu verstehen sind umm an die richtigen Daten zu kommen.

Markiere Inline querys indem du sie mit geschweiften Klammern umrandest. Für gewöhnlich verwenden wir einen *key* und einen *value* um eine Anfrage an die Datenbank zu stellen.

`[[player.{name:'Ada'}.score]]` liefert das selbe Item und den selben Wert wie die obige variable die mit einer Namens referenz funktioniert. Wir können mit querys aber noch mehr tun.

Zum beispiel können wir auch alle anderen Item Eigenschaften die ein Item im Laufe des game erhalten hat nutzen um es zu referenzieren.

`[[player.{location:'At the pub'}.score]]`

Gibt uns den `score` des `player` der oder die sich `At the pub` aufhält. Beachte, dass der *value*, solange es sich um einen *string* handelt in Anführungszeichen gesetzt werden muss.

Querys bieten viele weitere möglichkeiten Items zu referenzieren. Wenn du mehr erfahren willst schau dich einmal in der Dokumentation von [mongo DB](https://docs.mongodb.com/manual/reference/method/db.collection.find/) um, der Datenbank mit der adaptor:ex arbeitet.

### Item Container und Level Arguments

Wenn eine Session von einem Level gestartet wird ist es möglich dem Level Items als *Argumente* mitzugeben. Das heißt während des Level designs arbeiten wir mit Platzhaltern für Items, die erst dann referenziert werden, wenn eine Session im Live Modus gestartet wird.

Level Argumente können in der `level config` bearbeitet entfernt und hinzufüggt werden. Hier können wir auch bereits Items referenzieren, die in dem Falle in der level session eingesetzt werden, wenn beim start der level session kein alternatives item für dieses argument angegeben wurde.

![create level arguments](assets/variables_level_arguments.png)

Jedes level hat zu beginn bereits ein Argument `Player` definiert. `Player` ist in diesem Falle der Name des Arguments, wie es beim level design verwendet wird. Die weiteren angaben sind optional. Mit *collection* kann festgelegt werden aus welcher Datensammlung das Item, dass übergeben wird, stammen muss. *Default* ermöglicht es bereits festzulegen welches Item referenziert wird, wenn beim level session start kein Item übergeben wird. 

![create level arguments with default value](assets/variables_level_arguments2.png)

Beim start einer level session können auch argumente übergeben werden, die nicht in der `level config` definiert wurden.

Welches Item in einer neu gestarteten level session übergeben wird kann beim manuellen starten festgelegt werden. Auch die Control action [Launch Session](../basics/actions/control/launch.md) erlaubt es level arguments anzugeben.

Um im level design auf das variable item zuzugreifen verwenden wir den Namen des Arguments. Haben wir ein argument `Player` und wollen die telnumber Eigenschaft nutzen, nutzen wir, wie bei name und query referenzen, punkt notation.

Je nachdem welcher player item beim level session start manuell oder automatisch übergeben wurde bezieht sich `[[Player.telnumber]]` auf die telnumber Eigenschaft des jeweiligen player.

Mit der Data action [Load Item](../basics/actions/data/load.md) kannst du auch im laufe des Levels Items mit einem solchen container refernzieren. Das ermöglicht es auch, level arguments im laufe des levels zu überschreiben.

Manche Actions erlauben es Items die mit der action entstehen direkt in einen Container zu laden. Das Item wird unabhängig von der level session erstellt und ist auch zum session Ende noch vorhanden, es kann aber im level direkt mit dem angegebenen Referenz namen gelesen und geändert werden.

Plugins wie telegram und twilio nutzen Level Argumente wenn es eingehende Nachrichten von Kontakten gibt, für die bisher kein Level läuft. Wird so ein *default level* gestartet, wird überprüft ob ein player item mit entsprechenden Kontaktdaten (z.B. Telefonnummer) existiert um diesen player item an die level session als 'Player' argument weiterzugeben.
Existiert kein passendes player item wird ein neues erstellt und an die level session  weitergegeben.

Action Data
------------

Einige actions speichern Daten die anfallen ab und ermöglichen es so im Laufe des Levels darauf zuzugreifen.

Solche Action Daten sind über das keyword `state`, den statenamen, indem die action sich befindet und dem namen der action lesbar.

`[[state.MyState.smsin_1.text]]` enthält, sobald der state `MyState` getriggert wurde und eine sms eingegangen ist, den text dieser sms.

Das `VARIABLES` menu in der sidebar zeigt dir an welche deiner actions im laufe des levels Daten enthalten können und wie sie adressiert werden.

Functions
----------

Es ist auch möglich die Palette der [functions](./functions.md), die sich einfach erweitern lässt, zu nutzen um texte variabel zu ergänzen, bedingungen zu überprüfen oder Items zu verändern.

Eine function variable wird, wenn die entsprechende action ausgeführt wird, mit dem jeweiligen Rückgabe Wert (*return value*) der Funktion ersetzt.

`Es ist jetzt [[function.getTime()]] Uhr` ergibt also z.B. `Es ist jetzt 16:00 Uhr`.

Um argumente an die Funktion zu übergeben, werden sie, ohne leerzeichen, durch Komma getrennt zwischen die runden Klammern gesetzt.

`[[function.myFunction(arg1,arg2)]]`

Fehlende und leere Variablen
----------------------------

Führt eine Variable zu keinem Wert oder keinem Item ist sie **undefined**. Das kann der Fall sein, weil die refernzierte Eigenschaft oder das Item (noch) nicht existiert, oder weil die varibale einen (Tipp) Fehler enthät.

In einem text, der eine leere oder fehlende Variable enthält wird an dieser stelle auch **undefined** ergänzt. Etwa: 'Hallo [[Player.name]]!' würde, wenn kein Player existiert oder Player keine `name` Eigenschaft hat, zu `Hallo undefined!`