# adaptor:ex telegram plugin media files und andere Spezialfunktionen

adaptor:ex ist Teil des Projekts MACHINA COMMONS und wird im Programm "Digitale Entwicklung im Kulturbereich" der Berliner Senatsverwaltung für Kultur und Europa gefördert.

adaptor:ex ist eine open-source Software Engine, die es ermöglicht, interaktive Theatergames und -installationen umzusetzen. Ein Teilbereich davon sind inter(re)aktive Performances innerhalb des Messaging Services "Telegram".

## Tutorial

Dieses Tutorial ist Teil der <a href="https://gitlab.com/machina_ex/adaptor_ex/adaptor-ex-tutorials">Tutorialsammlung von adaptor:ex</a>.

Es baut auf dem ersten Tutorial <a href="../telegram-basics/index.md"> Create a simple telegram adventure with adaptor:ex</a> auf und setzt Wissen von dort vorraus. Wenn du das erste Tutorial noch nicht durchgearbeitet hast, nimm dir das bitte zuerst vor.

In diesem Tutorial lernen wir erweiterte Funktionen des Telegram Plugins kennen. Diese ermöglichen uns 

- Fotos
- Sprach/Audio Nachrichten
- Audio Files und andere Dateitypen
- Koordinaten inklusive einer Karte mit Pin zu diesen 

via unserer von adaptor:ex kontrollierten Telegram Clients zu versenden.

<div class="page"/>

# Schritt für Schritt

Du kannst mit einem leeren Level beginnen oder mit einem Level, in dem es bereits einige States und Actions gibt. In diesem Tutorial beginnen wir mit einem leeren Level:

<img src="./assets/0-1.png" width=80%>


## Bilder schicken


1. aus der **TOOLBAR** (links) unter dem Abschnitt Telegram ziehen wir nun die **ACTION** **sendFile** auf einen leeren Abschnitt der **STAGE** (rechts). Und verbinden die 3 **STATES** via **next**

    <img src="./assets/1-1.png" width=80%>

    <img src="./assets/2-1.png" width=80%>

2. Nun klappen wir die **sendFile** ACTION in unserem neuen State auf und sehen folgendes Formular:

    <img src="./assets/3-1.png" width=80%>

    Dort wählen wir unseren Client (z.B. Thekla) aus und unter **to** tragen wir 'Player' ein, da wir dies an die Spieler:in in dieser Session schicken wollen. (Später lernen wir auch wie wir hier statt verschiedener Player auch in einen Gruppenchat schicken können, aber das ist zuviel für dieses Tutorial)

3. Nun können wir ausserdem etwas unter **file** eintragen. Nämlich die Datei, die wir gerne an die Spieler:in schicken wollen. Die einfachste Art wäre, die Dateien zu benutzen, die unser Admin auf dem adaptor:ex server für dieses **Game** freigegeben hat. Wenn dort Daten liegen sollten, tauchen diese in der **TOOLBAR** auf. Wenn wir auf den Reiter/Tab **MEDIA** klicken, kann das in etwa so aussehen:

    <img src="./assets/4.png" width=40%>

    Diese Files liegen auf dem Server im Gameverzeichnis unter 'files/'. Je nachdem wie der adaptor:ex server konfiguriert wurde haben wir Zugriff, um Dateien selbst dort hochzuladen oder nicht. In unserem Beispiel liegen ein paar Dateien zum Testen auf den Server.

4. Diese können wir jetzt via drag and drop einfach in das Feld **file** ziehen:

    <img src="./assets/5-1.png" width=80%>
    <img src="./assets/6-1.png" width=40%>

5. Thekla (unser client) wird nun jeder Spieler:in, die das Level aktiviert, ein Foto schicken:

    <img src="./assets/7.png" width=80%>
    
    Photo by <a href="https://unsplash.com/@charlesdeluvio?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Charles Deluvio</a> on <a href="https://unsplash.com/s/photos/balloon-dog?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

6. Aber was wenn wir keinen Zugang zu dem adaptor:ex fileserver haben? Oder wenn wir einfach eine andere Datei verschicken wollen?

    Keine Sorge: Wir können einfach auch eine URL zu einer Datei aus dem Internet schicken. Die muss aber natürlich freigeben sein, um sie so zu benutzen.
    Zum Beispiel `https://machinacommons.world/img/logo-quer-700px.png`
 
    <img src="./assets/8.png" width=80%>

    Das funktioniert dann auch:

    <img src="./assets/9.png" width=80%>
  
  **Tipp:**

  Wenn wir Bilder schicken ist unter **format** die Einstellung `auto` perfekt. Das Bild wird, erfahrungsgemäß, solange es unter 10 MB groß ist, dann auch sofort eingeblendet, vorrausgesetzt die empfangende Person erlaubt dies in ihrer Telegram App. Bilder über 10MB werden den Empfangenden standardmäßig als Download angezeigt.

## Audio Files und Voice Messages

Wir können via Telegram auch Audio Dateien wie .mp3, .wav und .ogg verschicken.

.mp3 und .wav werden immer als Audio Downloadlink verschickt.

.ogg Dateien können als voice message (**format** zu `voice`) oder audio file verschickt werden. Wenn die .ogg Datei korrekt encoded ist (OPUS codec) und wenn sie nicht größer als 1MB ist, wird Telegram die Datei als audio message anzeigen, so als haetten wir sie mit Telegram selbst eingesprochen.

<img src="./assets/16.png" width=40%>

<img src="./assets/17.png" width=38%>

Wenn irgend etwas nicht korrekt ist für die voice message sendet Telegram die Datei als normalen Downloadlink. Die meisten kann mensch auch einfach mit einem Klick auf den Link direkt anhören.

### .mp3 zu .ogg vorbis mit opus codec umwandeln

Leider ist die Umwandlung von Sounddateien etwas komplex. Eine relativ einfache Möglichkeit bietet das Kommandozeilenprogramm **ffmpeg**. https://ffmpeg.org/download.html

auf MacOS benutzen wir das nach erfolgreicher Installation mit diesem Befehl im Terminal um die Datei input.mp3 in eine Datei output.ogg umzuwandeln:

<img src="./assets/19.png" width=80%>


## andere Files

Andere Dateitypen werden als Downloadlink verschickt, mit der Ausnahme einiger Video Dateien, die können mit **format** `video` auch im play live mode gesendet werden. Wie bei Bildern und Soundfiles, hängt aber die Darstellung nicht nur am Dateiformat und der Größe des Files, sondern auch an den Einstellungen der Telegram App der empfangenden Person.

## sende Kartenausschnitt mit Ort

1. um eine Karte zu senden, ziehen wir eine **sendGeo** ACTION aus der TOOLBAR auf die STAGE

    <img src="./assets/10-1.png" width=80%>

    <img src="./assets/11-1.png" width=80%>


2. sendGeo braucht neben **client** und **to** nur noch die Felder latitude (Breitengrad) und longitude (Längengrad) (ACHTUNG: type muss Number sein und beide sollten englisch notiert werden, also mit . anstatt eines Kommas!).
Latitude und longitude von einem Ort kannst du dir z.B. über Open Street Maps oder Google Maps raussuchen. Falls du nicht suchen möchtest, probier es doch mit diesen hier:
lat: 52.510105
long: 13.376661


    <img src="./assets/12-1.png" width=40%>
    <img src="./assets/13-1.png" width=45%>


3. Jetzt bekommt die Spieler:in eine digitale Straßenkarte mit Markierung zugesand, die sie auch direkt benutzen kann um ihr Navigationsprogramm zu öffnen. Das kann sehr nützlich sein für Theater-Performances/Führungen/Games, die die Spieler:innen an verschiedene Orte in der Stadt führen.

    <img src="./assets/14.png" width=35%>
    <img src="./assets/15.png" width=44%>
