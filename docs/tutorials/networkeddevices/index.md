# Devices (aka networked devices)

In vielen interaktiven Theatersituationen wollen wir externe Dinge an adaptor:ex anschließen. 

Wenn wir, zum Beispiel, ein interaktives Requisit wie die Bombe aus dem machina ex Stück 
'15 000 Gray' ansteuern wollen, müssen wir ihr Signale im HTTP Protokoll schicken...

NOTIZ: <span style="font-size:0.7em">*In dem folgenden Step by Step Tutorial gehen wir davon aus, dass wir mit einem Device mit HTTP Schnittstelle kommunizieren wollen, aber adaptor:ex unterstützt natürlich auch andere Netwerkprotokolle (TCP, UDP, UDP/OSC) als auch die Serielle (USB) Schnittstellen (vorrausgesetzt wir arbeiten in adaptor:ex im Local Mode).*</span>

## HTTP Requests schicken 

1. Nachdem wir unser Game ausgewählt oder erstellt haben, wählen wir Game > Settings aus
2. In den Settings klicken wir auf Devices und dort unter HTTP auf den Plus Button, um ein neues HTTP Device zu erstellen.

<img src='./assets/1.png'>
<img src='./assets/2.png'>

Devices existieren auf Game Ebene, also ein und dasselbe Device ist erreichbar aus jedem Level innerhalb eines Games.

3. Das neue Device bekommt einen Namen. In unserem Beispiel nennen wir es einfach "Bombe".

<img src='./assets/3.png'>

Unter diesem Namen wird es im Level Editor auffindbar sein, sowohl als Device als auch als Event Quelle (dazu spaeter mehr)

4. Unter Settings wählen wir nun "to Device" aus, denn wir wollen ja etwas an das Device Bombe schicken.

<img src='./assets/4.png'>

5. Hier gibt es potenziell mehr einzustellen. Aber wir halten es erst einmal simpel und füllen nur das Feld URL aus. Das ist die URL (z.B. `http://192.168.178.4:3000`) der Bombe in unserem Netzwerk.
Dann klicken wir SAVE.

<img src='./assets/5.png'>

NOTIZ: <span style="font-size:0.7em">*Weil wir die Bombe in unserem internen Netzwerk übers WLAN erreichen können, ist die URL in unserem Fall `http://` plus die IP Adresse der Bombe plus `:3000` weil unsere Bombe auf Port 3000 hört. Aber würde die Bombe könnte natürlich auch ein Device hinter einer domain sein (z.B. wenn sie über einen Internetservice angesteuert würde oder anderweitig DNS Auflösung stattfindet)*</span>

6. Nun erstellen wir ein Level oder wechseln in das Level aus dem wir unsere Bombe ansprechen wollen (GAME > OVERVIEW > LEVEL auswählen oder erstellen).

7. Im Level Editor finden wir nun links in der Action Bar, den Bereich "DEVICES" und die Action "Send Message".

<img src='./assets/6.png'>

8. Wir ziehen die Send Message Action auf unsere Bühne um einen neuen State mit der Send Message Action zu erstellen.

<img src='./assets/7.png'>

9. Wir nennen den State um (z.B. in TRIGGER_BOMB weil wir die Bombe starten wollen) und wählen unter "to" im Action Formular das Device "Bombe" aus, wenn es nicht schon ausgewählt ist.

<img src='./assets/8.png'>

10. Wir wissen aus der Dokumentation unseres Bomben Requisits, dass die Bombe gestartet werden kann mit einem GET Request an den endpoint/pfad `/AN`.


Also schauen wir uns unter Settings an, welche weiteren Felder wir in der Send Message Action auswählen könnten:

<img src='./assets/9.png'>

`message` ist die message die wir dem Device schicken wollen. Die Message wird in dem HTTP Request als sogenannte query parameter verschickt. Das brauchen wir im Fall der Bombe noch nicht, ist aber gut zu wissen.

`method` bestimmt welche HTTP methode angewandt wird. Wenn wir hier nichts angeben (und in den Settings nichts anderes eingestellt ist), wird ein HTTP GET Request versendet. Weil unsere Bombe ausschließlich auf GET Requests hört, brauchen wir das hier also nicht. Aber auch gut zu wissen.

`path` ist der path an den der HTTP Request geschickt wird. Aha! Die Bombe wird gestartet mit einer Nachricht an `/AN`. Also wählen wir `path` aus und schreiben `/AN` rein. 

<img src='./assets/10.png'>
    
Die send message Action schickt also, wenn wir in diesem State im Level ankommen, einen HTTP GET Request an die url der Bombe (`http://192.168.178.4:3000` wie wir in Step 5. bestimmt haben) und fügt noch den Pfad `/AN` an die url an (also geht unsere Nachricht an `http://192.168.178.4:3000/AN` in unserem Beispiel)
    
<br/>

11. Wir verbinden also unseren neuen State (fügen  natürlich noch eine NEXT Action ein, etc.), klicken oben rechts auf den LIVE MODUS Button und starten eine Session.
<img src='./assets/11.png'>
Sobald die Session bei unserem State `TRIGGER_BOMB` ankommt, wird unsere send message Action aktiviert und wir schicken ein HTTP GET Request an unsere Bombe und starten sie.
Huch! Es tickt! Jetzt aber schnell!!!

<br/>

Perfekt. Wir wissen jetzt also wie wir ein neues Device erstellen und ihm HTTP requests senden! Herrvorragend!

Aber was, wenn wir ein Device haben, dass unserem adaptor:ex server/computer HTTP requests senden soll?

<br/>

## HTTP Requests empfangen - mit Webhooks

Unser Beispielrequisit, die Zeitbombe aus 15'000 Gray, schickt im Fall dass ihr Timer abgelaufen ist (Bumm!) als auch in dem Fall dass sie vom Publikum erfolgreich entschärft wurde (Phew!) einen HTTP GET REQUEST an die IP Adresse des Computers auf dem adaptor:ex läuft.

Damit adaptor:ex auf diese Nachrichten reagiert, müssen wir zurück die Game Settings:

1. Dort wählen wir den Settings Button des Devices Bombe aus und sehen dass es neben to_device auch `from-device` gibt. Das wählen wir aus.
<img src='./assets/15.png'>

2. Weil wir von der Bombe GET requests erwarten, ändern wir nichts, sondern klicken einmal auf SAVE und dann auf den Reload Button oben rechts (das Zirkel Symbol)
<img src='./assets/12.png'>
<img src='./assets/14.png'>

Nach einem kurzen Reload sollte nun der Indikator grün leuchten und wir sehen eine URL im Feld `webhook`. An diese URL sollte nun unsere Bombe ihre Signale schicken. 

3. Wir konfigurieren unsere Bombe (oder jedes andere Device) also so dass es an unsere webhook adresse schickt. 
In unserem Beispiel schickt die Bombe nun einen HTTP GET Request mit dem query parameter `?timeout=true` wenn sie explodiert oder `?solved=true` wenn sie entschärft wurde. 

 Wir wollen also nun auf diese beiden Events reagieren.

4. Wir wechseln zurück in den Level Editor und draggen eine `ON EVENT` ACTION auf die Stage
<img src='./assets/16.png'>

5. Dort finden wir jetzt `Bombe` im drop down menu unter `default events`. Wir wollen also, dass etwas passiert, wenn das Event Bombe eintritt. 
Der einfachheit halber wählen wir erst einmal unter `then` die Option `Go to next state` und verknuepfen diese Listener Action nun mit `QUIT`
<img src='./assets/17.png'>
<img src='./assets/18.png'>

6. Wenn die Bombe jetzt also einen HTTP GET request an ihren `webhook` schickt, wird das Event ausgelöst und wir gelangen am Ende des Levels an. Im Log des adaptor:ex servers können wir das auch gut nachvollziehen (wenn uns die Technik da reinschauen lässt):


### Filter incoming HTTP REQUESTS


Aber wie unterscheiden wir jetzt zwischen einem request mit `?timeout=true` und einem mit `?solved=true`?

7. statt wie in Schritt 5. `Go to next state` wählen wir `Switch Event Message` aus. Die url queries fallen direkt in das Event rein. So können wir die einzelnen queries (`timeout` und `solved`) direkt von adaptor:ex anschauen und überprüfen lassen um dann zu bestimmen welcher State in welchem Fall ausgelöst werden darf. 
Wir fügen also eine condition hinzu und tragen in das feld `field` ein nach welchem query adaptor:ex suchen soll (`timout` oder `solved`) und welchen Wert dieser query parameter haben muss damit etwas passiert (`true` in unserem Beispiel).
Schließlich tragen wir unter `next state` den Namen des states ein, der eintreten soll, wenn dies der Fall ist. 
<img src='./assets/19.png'>

Nachdem wir dann noch einen WIN State angelegt haben (der könnte später zum Beispiel Die Siegesmusik abspielen oder Licht ansteuern etc.),sieht das Level jetzt ungefähr so aus:

<img src='./assets/19a.png'>

<img src='./assets/21.png'>

Und wenn wir in den Live Modus wechseln und eine neue Session starten und diese dann markieren, sehen wir: Die Session bleibt bei dem State `ON_BOOM` stehen und wartet auf die von uns definierten Events.

<img src='./assets/20.png'>

Wenn das Bomben Requisit jetzt einen HTTP GET Request an unseren webhook schickt und `?solved=true` mit schickt, sehen wir wie der WIN State ausgelöst wird.

<img src='./assets/22.png'>

NOTIZ: <span style="font-size:0.7em">*Es gibt noch mehr Möglichkeiten den Inhalt des Bombe Events auszulesen (z.b. finden wir die Daten des onEvents auch in der ACTION BAR unter Variables und können ihn dort dann weiter bearbeiten etc.)*</span>

So können wir jetzt unsere Requisiten mit adaptor:ex reden lassen als auch von adaptor:ex Signale an externe Devices schicken.

Natürlich lassen sich so auch andere Software im Netzwerk und auf demselben Computer ansteuern und sogar 3rd party Dienste im Netz (obwohl wir für letzteres meistens empfehlen, lieber einen eigenen Plugin zu programmieren, wenn das möglich ist)
