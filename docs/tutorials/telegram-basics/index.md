# Storytelling mit Telegram und adaptor eX

adaptor:ex ist Teil des Projekts MACHINA COMMONS und wird im Programm "Digitale Entwicklung im Kulturbereich" der Berliner Senatsverwaltung für Kultur und Europa gefördert.

adaptor:ex ist eine open-source Software Engine, die es ermöglicht, interaktive Theatergames und -installationen umzusetzen. Ein Teilbereich davon sind inter(re)aktive Performances innerhalb des Messaging Services "Telegram".

## Vorab:

Wir gehen davon aus dass du oder eine Techniker:in bereits einen Telegram Client mit default level eingerichtet hat. (<a href="../telegram-settings/index.md" target="_blank" rel="nofollow noreferrer">Tutorial</a>)

adaptor:ex kann zu großen Teilen mit einem Browser (wir empfehlen Chromium/Chrome oder Firefox) bedient werden.
Um dein Level zu testen, brauchst du natürlich den Messenger Telegram, entweder als App auf deinem Smartphone oder auf deinem Computer.

<div class="page"/>

## Schritt für Schritt

### Grundlagen

1. Wenn du dich im GAME OVERVIEW befindest, klicke auf das Level (blauer Button), das du bearbeiten möchtest …     

<img src="./assets/levelview.png" alt="screenshot"/>

2. … um in den Level Editor zu gelangen:

<img src="./assets/leveleditor-1.png" alt="screenshot leveleditor"/>

3. Um die Grundlagen des LEVEL EDITOR kennen zu lernen, solltest du dir die folgende Erklärung ansehen:
<a href="../../basics/editor.html" target="_blank" rel="nofollow noreferrer">Einführung Editor</a>
Es bietet sich an, den Link geöffnet zu lassen, falls du während des Tutorials nochmal nachschauen möchtest.

### Getting started

1. Wir fangen mit etwas Einfachem an: Scroll in der Toolbar unter ACTIONS runter, bis du unter TELEGRAM **sendMessage** siehst. Dann zieh mit der Maus (drag and drop) die sendMessage ACTION auf einen freien Bereich der Stage.
     
     <img src="./assets/actions3-2.png" alt="screenshot"/>

    Jetzt kannst du den neu entstandenen State umbenennen. Wir nennen ihn mal WILLKOMMEN:
     
     <img src="./assets/actions4-3.png" width="350px" alt="screenshot"/>

    STATES lassen sich auf der Stage via drag and drop bewegen: Einfach mit dem Mauszeiger über der Kopfzeile hovern, klicken und dann bewegen.

2. Jetzt wollen wir daran arbeiten, dass in unserem Level auch was passiert. Dazu müssen wir den START state mit unserem neuen state WILLKOMMEN verbinden. 

    Dazu öffnen wir die NEXT ACTION, die im START state schon bereit liegt, indem wir auf die Kopfzeile der NEXT ACTION klicken. Klicke in das Textfeld unter "next state" - du kannst hier entweder den Namen des STATES schreiben, der auf den START state folgen soll oder ihn aus dem Dropdown auswählen – in unserem Fall also "WILLKOMMEN". Danach klicken wir einmal irgendwo auf die Stage – und fertig. Nun geht es für Spieler:innen vom START sofort in unseren neuen state WILLKOMMEN:

     <img src="./assets/actions6-2.png" alt="screenshot"/><br>
     <img src="./assets/actions7-2.png" alt="screenshot"/>

3. In WILLKOMMEN wollen wir jetzt eine Nachricht an die Spieler:in schicken, die unser Level gestartet hat. 
    Dazu öffnen wir die **sendMessage** ACTION und füllen das Formular wie folgt aus:

     <img src="./assets/actions9-2.png" width=50% alt="screenshot"/>

    Der TELEGRAM CLIENT ist gewissermaßen unsere Figur, der Absender unserer Nachrichten. In unserem Workshop-Level gibt es nur eine Figur, aber in komplexeren Games könnten wir auch mehrere Charaktere haben, die mit unseren Spieler:innen kommunizieren.
    
    Unter "to" wählen wir "**Player**" aus. So entscheiden wir, an wen die Telegram-Nachricht gesendet wird. Auch hier haben wir nur eine Spieler:in zur Auswahl. Es könnten aber auch mehrere sein, z.B. eine Gruppe.

    Unter "**text**" können wir einen kleinen Text schreiben (den Inhalt der Nachricht).

4. Jetzt ziehen wir eine **next** ACTION in unseren "Willkommen" STATE, öffnen diese und tragen QUIT unter "next state" ein.

    <img src="./assets/actions8-3.png" alt="screenshot"/>

    <br><br>
    <img src="./assets/actions10-1.png" width=30% alt="screenshot"/>

    Nun sollte unser Level auch schon funktionieren.
     
    So sollte das jetzt ungefähr aussehen:

    <img src="./assets/actions11-1.png" alt="screenshot"/>

<div class="page"/>

5. Jetzt sollten wir mal testen, ob alles funktioniert. Dazu schicken wir den Namen unseres Levels als Telegram-Nachricht an Thekla, unsere Testfigur. Dazu solltest du Thekla als Kontakt anlegen. Die Nummer von Thekla findest du unter Game > Settings > Telegram > Client. 


     <img src="./assets/telegram1.png" width="300px" alt="screenshot"/>

    Wenn alles geklappt hat, sollte Thekla antworten: Hallo, wie heißt du?
    (Die Weiterleitung ist für unseren Workshop in einem übergeordneten Menu Level vorbereitet, deshalb bekommst du noch eine weitere Antwort zurück - die kannst du erstmal ignorieren.)
    Hat es funktioniert? Dann **Herzlichen Glückwunsch!**



### Testing mit Session Ansicht

Wenn es Probleme gibt, klicke am besten als Erstes oben auf das kleine Dreieck in der rechten Ecke. Damit öffnest du die SESSION Ansicht.
Wenn eine Session im Gange ist, oder auch wenn etwas kaputt gegangen ist oder feststeckt, siehst du die Session dort oben und kannst sie manuell beenden, indem du auf den Mülleimer klickst.
Wenn du gerade keine Session sehen kannst, ist das ganz normal und liegt daran, dass diese bereits wieder beendet ist. Sende doch einfach noch einmal den Levelnamen an Thekla, um die Session zu sehen, während sie aktiv ist. 

Vergiss aber nicht, danach die Session Ansicht zu schließen!

<div class="page"/>

### User-Eingaben verwerten

Nun passiert erstmal nichts weiter in unserem Level. Es startet, die Spieler:in bekommt unsere Nachricht und dann endet es auch schon. 
Um Interaktion hinzuzufügen benutzen wir eine neue ACTION in einem neuen state:

Wir ziehen aus den TELEGRAM ACTIONS in der Toolbar eine DIALOG ACTION auf einen leeren Teil der Stage.


Wir wollen nun den Inhalt der Nachricht prüfen, die die Spieler:in an Thekla sendet, wenn sie an der ACTION dialog ankommt. Die Action dialog macht genau das: Das System wartet auf eine Eingabe der Spieler:in und überprüft dann nach von uns zu definierenden Regeln, wie es mit der Antwort der Spieler:in umgehen soll.


1. Benenne den neuen State, wie du willst, zum Beispiel "WAITING" (so nennen wir ihn in diesem Tutorial ab jetzt), und fülle ihn wie folgt aus:

2. Klicke in der DIALOG ACTION auf "Settings" und wähle "else" aus. Dann klicke zum Schließen wieder auf "Settings". ("if" kommt später, keine Sorge)

    <img src="./assets/dialog1-1.png" alt="screenshot" width="350px"/><br>

3. Wir sehen nun, dass sich ein else-Block in der ACTION zeigt. Dazu kommen wir gleich.

    Außerdem sehen wir dass, wenn wir in der Toolbar auf VARIABLES klicken, neue VARIABLEN aufgetaucht sind: Es gibt eine ganze Reihe von Boxen, deren übergeordnete Box so wie dein dialog-STATE benannt ist. In unserem Tutorial heißt sie "WAITING", darunter steht "dialog-1". **Eventuell musst du die Seite neu laden, um die neuen Variablen zu sehen.**

    <img src="./assets/dialog2-1.png" alt="screenshot" width="250px"/>
    <br>

    Hier finden wir ab sofort die Daten (z.B. die Eingaben der Spieler:innen), die uns aus dem State WAITING zur Verfügung stehen (natürlich immer erst dann, wenn das Level an dem STATE namens "WAITING" angekommen ist).
    
    <img src="./assets/dialog6-1.png" alt="screenshot"/>
    
4.  Wir haben die Spieler:in nach ihrem Namen gefragt und wollen sie ab sofort auch korrekt ansprechen, also benutzen wir den Antworttext, den die Spieler:in an unseren Client Thekla gesendet hat: Wir ziehen mit der Maus die VARIABLE **text** aus dem "dialog_1" in das Textfeld "**response**" des else-Blocks, wie im oberen Bild zu sehen.

    Im Textfeld taucht jetzt [[state.WAITING.dialog_1.text]] in eckigen Klammern auf. Die eckigen Klammern markieren Variablen und ähnliche Daten. (Wir könnten natürlich auch einfach selbst die Variable in das Textfeld tippen, aber drag & drop bewahrt uns vor Tippfehlern.)

<div class="page"/>

5. Schreibe nun einen Grußtext, zum Beispiel "Hallo" vor und/oder hinter die Variable.

    <img src="./assets/dialog3-1.png" alt="screenshot" width="500px" />

    Alles innerhalb der eckigen Klammern wird im Spiel ersetzt. Alles ohne eckige Klammern wird genau so als Antwort (**response**) geschickt, wie es im Textfeld steht.

6. Trage nun unter **else** > **next state** QUIT ein, damit das Level auch automatisch beendet wird.
    Du kannst jetzt auch endlich wieder testen: Thekla sollte jetzt, wenn du ihr bei Telegram eine Nachricht schreibst, wie folgt antworten:

    <img src="./assets/telegram2.png" alt="screenshot" width=50%/>    

    Wie du siehst, speichert das Level deine Eingabe, wenn du am state "WAITING" ankommst, und kombiniert den Text dann im response. Yay!

7. Um nur auf bestimmte Eingaben und nicht einfach auf jede denkbare Eingabe zu reagieren, brauchen wir neben der "else"-Bedingung noch eine "if"-Bedingung: Öffne die **dialog_1 ACTION** innerhalb des states "WAITING". Nun klicke wieder auf "**Settings**" vom **Telegram client** und füge "**if**" hinzu:

    <img src="./assets/dialog5-1.png" alt="screenshot" width="300px" />

8. Jetzt taucht innerhalb der ACTION oberhalb des else-Blocks ein if-Block auf.
    Wähle im oberen Drop-Down-Menü "**Contains**", wenn es nicht schon ausgewählt ist. Nun kannst du darunter im contains-Block im Textfeld eingeben, was das magische Zauberwort sein soll, um Thekla zum Schweigen zu bringen. Zum Beispiel "ruhe".
    Und unter "next state" trägst du ein, welcher State eintreten soll, wenn die eingehende Nachricht "ruhe" beinhaltet . Zum Beispiel QUIT.

    Trag unter **respond** den Text ein, mit dem Thekla antworten soll, bevor der next state eintritt, zum Beispiel "ok. ich gebe Ruhe!". 

    <img src="./assets/dialog4-1.png" alt="screenshot" width=40%/>

    Jetzt hast du einen Filter mit if-else im dialog gebaut:
    Wenn du Thekla nun irgendeinen Text schickst, der die Buchstabenfolge "ruhe" enthält (if), wird sie antworten: "ok. ich gebe Ruhe!", auf alle anderen Antworten (else) wird Thekla antworten wie schon zuvor.
    Du kannst so viele if-conditions hinzufügen, wie du willst, und auf diese Weise riesige interaktive Erzählungen bauen.

### weitere Telegram Spezialfunktionen

Jetzt haben wir die Grundfunktionen und das Interface von adaptor:ex kennengelernt und unser erstes Telegram-Level mit den Telegram ACTIONS "sendMessage" und "dialog" gebaut.
In der Toolbar (links) unter ACTIONS finden sich aber noch weitere Telegram ACTIONS mit Spezialfunktionen:

1. **sendFile**

    sendFile kann Bilder, Sounddateien und Dokumente verschicken. Es funktioniert ähnlich wie sendMessage.
    Unter file können wir Dateien aus der Toolbar > MEDIA droppen  - oder wir können die URL zu Dateien, die im Internet erreichbar sind, verlinken, indem wir die Webadresse (http:// oder https://) eingeben. (Zum Beispiel Fotos von https://workshopics.imgbb.com/ deren Link wir mit Rechtsklick > Linkadresse kopieren können).
    
2. **sendGeo**

    sendGeo funktioniert ähnlich, braucht aber die Felder latitude (Breitengrad) und longitude (Längengrad) ausgefüllt (ACHTUNG: type muss Number sein). Damit schicken wir eine digitale Straßenkarte mit Markierung. Das kann sehr nützlich sein für Shows/Games, die die Spieler:innen an verschiedene Orte in der Stadt führen.

Es gibt noch viele weitere ACTIONS und Spezialfunktionen, wir können auch unsere eigenen Variablen definieren und später mit [[meineVariable]] in eckigen Klammern abfragen und und und...

------------

## weiterführende Infos

Mehr Infos erhaltet ihr unter:

https://www.machinacommons.world
oder unter

info@machinaex.de
www.machinaex.de


Da adaptor:ex bis Ende 2021 noch in Entwicklung ist, ändern sich Dinge noch ein wenig. Die Grundfunktionen und Bedienung werden aber dieselben bleiben und sich nur noch erweitern. Und da es ein open source Projekt ist, mit dem wir in Zukunft auch weiter arbeiten werden, wird das auch so bleiben. 

adaptor:ex kann außer Telegram auch noch viel mehr: Bühnenlicht kontrollieren, interaktive Requisiten ansteuern, mit anderer Software (Lichtprogrammen, Soundprogrammen, etc.) kommunizieren uvm. 
